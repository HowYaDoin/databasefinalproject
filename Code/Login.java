import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
/**
 * handles loggin into the database and passes user name to audit
 * @author Kelsey Pereira
 */
public class Login {
    //private boolean loginCheck;
    private String username;
    public Login(String username){
        this.username = username;
    }
    /**
     * get method for username
     * @return
     */
    public String getUsername() {
        return username;
    }
}