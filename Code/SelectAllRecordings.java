import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import javafx.scene.control.TextArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
/**
 * Handler for the button that triggers the subprogram SelectAllRecordings
 * @author Bastian Fernandez Cortez
 */
public class SelectAllRecordings implements EventHandler<ActionEvent>{
    private Connection connection;
    private String result = "";
    private AddToAudit audit;
    private TextArea ta;
    public SelectAllRecordings(Connection connection, AddToAudit audit, TextArea ta) throws SQLException{
        this.connection = connection;
        this.audit = audit;
        this.ta = ta;
    }
    @Override
    public void handle(ActionEvent arg0){
        System.out.println("Fetching...");
        String query = "call selectData_package.SELECTALLRECORDINGS(?)";
        CallableStatement call;
            try{
                this.audit.addToAuditTable("Select");
            } catch(SQLException e) {
                System.out.println("Could not add to audit table");
            }

        try {
            call = this.connection.prepareCall(query);
            call.registerOutParameter(1, Types.VARCHAR);
            call.execute();
            this.result = call.getString(1);
            call.close();
            if (this.result == null){
                throw new SQLException();
            }
            ta.setText(this.result);
            System.out.println(this.result);
        } catch (SQLException e) {
            ta.setText("" + e);
        }
        
    }
}
