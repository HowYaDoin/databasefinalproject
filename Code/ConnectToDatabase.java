import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * connects to the database
 * @author Kelsey Pereira
 */
public class ConnectToDatabase {
    public Connection getConnection(String username, String password) throws SQLException{
        String URL = "jdbc:oracle:thin:@pdbora19c.dawsoncollege.qc.ca:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection connection = DriverManager.getConnection(URL, username, password);
        //this.connection = connection;
        return connection;
    }
}
