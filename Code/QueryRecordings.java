import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
/**
 * queries the recordings in the database
 * @author Kelsey Pereira Costa
 */
public class QueryRecordings implements EventHandler<ActionEvent>{
    private AddToAudit audit;
    private TextArea textArea;
    private Connection connection;
    PreparedStatement queryStatement;
    public QueryRecordings(AddToAudit a, TextArea ta, Connection c){
        this.audit = a;
        this.textArea = ta;
        this.connection = c;
        String queryString = "SELECT * FROM recording";
        try{
            this.queryStatement = this.connection.prepareStatement(queryString);
        } catch (SQLException e){
            throw new IllegalArgumentException("Could not fetch table recording");
        }
    }
      /**
     * contructs a table using a string builder from information from recording table
     * @return
     */
    private String constructTable(){
        try{
            ResultSet rs = this.queryStatement.executeQuery();
            StringBuilder sb = new StringBuilder("Recording Table\n");
            while (rs.next()){
                sb.append(rs.getString("RecordingId") + " | ");
                sb.append(rs.getString("RecordingDate") + " | ");
                sb.append(rs.getString("RecordingDuration") + " |\n");
            }
            return sb.toString();
        } catch (SQLException e){
            return "Did not find table";
        }
    }
    @Override
    public void handle(ActionEvent arg0) {
        String table = constructTable();
        this.textArea.setText("");
        this.textArea.setText(table);
    try{
        this.audit.addToAuditTable("Query");
    } catch(SQLException e) {
        System.out.println("Could not add to audit table");
    }
        
    }
}
