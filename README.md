# Music Database Manager

A database manager that handles music recordings, compilations
Able to add information, Update and view information from the database

## Installation

Unzip the zip file and open DatabaseApplication with [VsCode](https://code.visualstudio.com/)
In vscode, Make sure to download/have downloaded the Java extension by RedHat

If you are running this in the Dawson Lab Machines: Go to the bottom of database application and click run

If you are running this at home: open the vsCode launch file and modify the file path to point to a folder which holds all the JavaFx jar libraries

```json
"type": "java",
"name": "Launch DatabaseApplication",
"request": "launch",
"mainClass": "DatabaseApplication",
"projectName": "databasefinalproject_e247fe17",
"vmArgs" : ["--module-path", "<Path here>", "--add-modules=javafx.controls" ]
```
Make sure the path is **absolute**

Finally, import ojdbc.jar and all javafx jar files into **Java projects/Referenced libraries** at the bottom of the vscode file hierarchy 

## Usage
### Once the GUI application opens:
Log into the database with your name. The rest of the program should appear

If you are confused as to how to add or update, click on the buttons and some options will appear. Use those options and put them into the User input field

## Credits
### Bastian Fernandez Cortez
- Everything in packages.sql except the delete package
- Select function
- Create tables and drop tables
- Database design
- JavaFx
- Login function (Java)
- Debugging
### Kelsey Pereira Costa
- Java button functionality (Add and Update)
- Link subprograms to Java GUI
- Audit table functionality (Java side)
- JavaFx
- Java structure Design
- query recordings and contributor buttons
- Team Organization
### Shay Alex Lelichev
- Delete subprogram package
- ERD diagram
