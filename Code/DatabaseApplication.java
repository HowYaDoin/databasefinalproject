import java.sql.Connection;
import java.sql.SQLException;
import javafx.application.*;
import javafx.scene.paint.*;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.*;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;

/**
 * application class for the gui
 * @author Kelsey Pereira and Bastian Fernandez Cortez
 */
public class DatabaseApplication extends Application { 	
    //private DatabaseTableManager DTM;
    private Connection connection;
    @Override
    public void start(Stage stage) { 
    //container and layout
    BorderPane root = new BorderPane(); 
    //root.setBackground(new Background(new BackgroundFill(Paint.valueOf("#4C5B5C"), CornerRadii.EMPTY, Insets.EMPTY)));
    //connects to database
    ConnectToDatabase CTD = new ConnectToDatabase();
    /*Parameters params = getParameters();
    List<String> connectiontodata = params.getRaw();*/
    
    try{
        this.connection = CTD.getConnection("A2041556", "Arceus5417");
    } catch (SQLException e){
        System.out.println("Could not establish connect for some reason" + e);
    }
    System.out.println("successfully connected");
    VBox userInputBox = new VBox();
    ScrollPane scroll = new ScrollPane(userInputBox);
    root.setCenter(scroll);
    scroll.setFitToHeight(true);
    scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
    //Text nameText = new Text("Type in a username and click log-on");
    TextField nameInput = new TextField("John Glee");
    Button logOnButton = new Button("Log-On");
    TextField userInputText = new TextField();
    VBox bigBox = new VBox();
    LoginButton loginButtonHandler = new LoginButton(nameInput, userInputText ,this.connection, userInputBox);
    logOnButton.setOnAction(loginButtonHandler);

    userInputBox.getChildren().addAll(nameInput,logOnButton,userInputText,bigBox);
    root.getChildren().add(userInputBox);
/*
*/
    //scene is associated with container, dimensions
    Scene scene = new Scene(root, 500, 400); 
    scene.setFill(Paint.valueOf("#4C5B5C"));

    //associate scene to stage and show
    stage.setTitle("Hello JavaFX"); 
    stage.setScene(scene); 
    stage.show(); 

    } 
    public static void main(String[] args) {
        DatabaseApplication.launch(args);
    }    
} 