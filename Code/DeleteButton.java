import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * handles delete button functionality
 * @author Shay Alex Lelichev
 */
public class DeleteButton implements EventHandler<ActionEvent>{
    private AddToAudit audit;
    private TextField tf;
    public DeleteButton(AddToAudit a, TextField t){
        this.audit = a;
        this.tf = t;
    }
    @Override
    public void handle(ActionEvent arg0) {
    try{
        this.audit.addToAuditTable("Delete");
    } catch(SQLException e) {
        System.out.println("Could not add to audit table");
    }
    }
}
