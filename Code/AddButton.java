import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * event handler class that controls the add button
 * @author Kelsey Pereira
 */
public class AddButton implements EventHandler<ActionEvent>{
    private AddToAudit audit;
    private TextField tf;
    private VBox optionBox;
    private Connection connection;
    public AddButton(AddToAudit a, TextField t, VBox box, Connection con){
        this.audit = a;
        this.tf = t;
        this.optionBox = box;
        this.connection = con;
    }
    @Override
    public void handle(ActionEvent arg0) {
        VBox options = (VBox)this.optionBox.getChildren().get(1);
        TextArea display = (TextArea)this.optionBox.getChildren().get(2);
        options.getChildren().clear();
        System.out.println(this.tf.getText());
        Button submit = new Button("Submit");
        options.getChildren().add(submit);
        if(this.tf.getText().equals("recording")) {
            this.tf.setText("");
            this.tf.setText("recordingType;date;duration");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call addData_package.addRecordingCompilationData(?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("contributor")){
            this.tf.setText("");
            this.tf.setText("name");
            display.setText("Input required information");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String dataArr = tf.getText();
                    String sql = "call addData_package.addContributor(?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("role")){
            this.tf.setText("");
            this.tf.setText("name");
            display.setText("Input required information");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String dataArr = tf.getText();
                    String sql = "call addData_package.addRole(?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("performance")){
            this.tf.setText("");
            this.tf.setText("recordingId;roleName;contributorId");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call addData_package.addPerformance(?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("component")){
            this.tf.setText("");
            this.tf.setText("vRecordingId;vRecordingId2;compModifiedOffset;compModifiedDuration;compOriginalOffset;compOriginalUsed");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call addData_package.addPerformance(?,?,?,?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.setString(1, dataArr[3]);
                        call.setString(2, dataArr[4]);
                        call.setString(3, dataArr[5]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("merch")){
            this.tf.setText("");
            this.tf.setText("VRECORDINGID;VRELEASEDATE;VLABEL;VTITLE;VMARKETNAME;");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call addData_package.ADDMERCHDATA(?,?,?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.setString(1, dataArr[3]);
                        call.setString(2, dataArr[4]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else {
            display.setText("Table does not exist\nOptions are:\n-recording\n"+
            "-contributor\n"+
            "-role\n"+
            "-performance\n"+
            "-component\n"+
            "-merch");
            options.getChildren().clear();
        }

        try{
            this.audit.addToAuditTable("Add");
        } catch(SQLException e) {
            System.out.println("Could not add to audit table");
        }
    }
}