import java.sql.Connection;
import java.sql.SQLException;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
/**
 * login button that when pressed will display the rest of the gui application
 * @author Bastian Fernandez Cortez
 */
public class LoginButton implements EventHandler<ActionEvent>{
    private boolean loginCheck;
    private Login user;
    private TextField textField;
    private TextField userInput;
    private AddToAudit audit;
    private Connection connection;
    private VBox userInputbox;
    public LoginButton(TextField username, TextField userInput, Connection con, VBox userInputBox){
        this.textField = username;
        this.userInput = userInput;
        this.user = new Login(username.getText());
        this.loginCheck = false;
        this.connection = con;
        this.audit = new AddToAudit(this.user, this.connection);
        this.userInputbox = userInputBox;
    }
    /**
     * returns login object
     * @return
     */
    public Login getUser() {
        return user;
    }
    /**
     * returns audit object
     * @return
     */
    public AddToAudit getAudit() {
        return audit;
    }
    
    @Override
    public void handle(ActionEvent arg0) {
        VBox bigBox = ((VBox)this.userInputbox.getChildren().get(3));
        VBox buttonOptionBox = new VBox();
        if (loginCheck) {
            try{
                this.audit.addToAuditTable("Logged off");
                bigBox.getChildren().clear();
            } catch(SQLException e) {
                System.out.println("Could not add to audit table");
            }
                //System.out.print("we true baby:loged off " + this.user.getUsername());
                loginCheck = !loginCheck;
            } else {
                this.user = new Login(this.textField.getText());
                this.audit.setLoginName(this.user);
        HBox buttonBox = new HBox();
        Button queryButton = new Button("Search");
        //Query queryButtonHandler = new Query(loginButtonHandler.getAudit(), userInputText, root);
        //queryButton.setOnAction(queryButtonHandler);
    
        Button addButton = new Button("Add");
        AddButton addButtonHandler = new AddButton(this.audit, (TextField)userInputbox.getChildren().get(2), bigBox, this.connection);
        addButton.setOnAction(addButtonHandler);
        
        Button updateButton = new Button("Update");
        UpdateButton updateButtonHandler = new UpdateButton(this.audit, (TextField)userInputbox.getChildren().get(2),bigBox, this.connection);
        updateButton.setOnAction(updateButtonHandler);
        
        Button deleteButton = new Button("Delete");
        DeleteButton deleteButtonHandler = new DeleteButton(this.audit, (TextField)userInputbox.getChildren().get(2));
        deleteButton.setOnAction(deleteButtonHandler);
        //Button logOffButton = new Button("Log-Off");
        
        buttonBox.getChildren().addAll(addButton,updateButton,deleteButton,queryButton);
        TextArea displayArea = new TextArea("display\narea");
        displayArea.setPrefHeight(200);
        displayArea.setEditable(false);
        
        VBox buttonArea = new VBox();
        Button recordingContributorsData = new Button("(R/C####) Contributor and their roles for a recording/compilation");
        Button contributorsComponent = new Button("(C####) Contributor and their roles for a compilation's components");
        Button recordingDetails = new Button("(R/C####) Shows the date and duration of a recording, as well as the Compilations it is a part of. If a compilation is chosen, it calls selectComponentDetails");
        Button recordingRolesData = new Button("(CO###) Lists all of the recordings/compilations a contributor has participated in and their roles in each of them.");
        Button allRolesOfContributor = new Button("(CO###) Shows all the roles of a contributor");
        Button allRecordings = new Button("Shows all the recordings in the recordings table");
        Button allCompilations = new Button("Shows all the compilations in the recordings table");
        Button allMarketedRecordings = new Button("Shows all the recordings in the Merch table");
        Button allMarketedCompilations = new Button("Shows all the compilations in the Merch table");

        //Actions for the buttons above
        SelectRecordingContributorsData SRCD;
        SelectContributorsComponent SCC;
        SelectRecordingDetails SRD;
        SelectRecordingRolesData SRRD;
        SelectAllRolesOfContributor SAROC;
        SelectAllRecordings SAR;
        SelectAllCompilations SAC;
        SelectAllMarketedRecordings SAMR;
        SelectAllMarketedCompilations SAMC;
        try {
            SRCD = new SelectRecordingContributorsData(this.connection, this.userInput, this.audit, displayArea);
            recordingContributorsData.setOnAction(SRCD);
            SCC = new SelectContributorsComponent(this.connection, this.userInput, this.audit, displayArea);
            contributorsComponent.setOnAction(SCC);
            SRD = new SelectRecordingDetails(this.connection, this.userInput, this.audit, displayArea);
            recordingDetails.setOnAction(SRD);
            SRRD = new SelectRecordingRolesData(this.connection, this.userInput, this.audit, displayArea);
            recordingRolesData.setOnAction(SRRD);
            SAROC = new SelectAllRolesOfContributor(this.connection, this.userInput, this.audit, displayArea);
            allRolesOfContributor.setOnAction(SAROC);
            SAR = new SelectAllRecordings(this.connection, this.audit, displayArea);
            allRecordings.setOnAction(SAR);
            SAC = new SelectAllCompilations(this.connection, this.audit, displayArea);
            allCompilations.setOnAction(SAC);
            SAMR = new SelectAllMarketedRecordings(this.connection, this.audit, displayArea);
            allMarketedRecordings.setOnAction(SAMR);
            SAMC = new SelectAllMarketedCompilations(this.connection, this.audit, displayArea);
            allMarketedCompilations.setOnAction(SAMC);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        buttonArea.getChildren().addAll(recordingContributorsData,contributorsComponent,recordingDetails,recordingRolesData,
        allRolesOfContributor,allRecordings,allCompilations,allMarketedRecordings,allMarketedCompilations);
        bigBox.getChildren().addAll(buttonBox,buttonOptionBox,displayArea,buttonArea);
        try{
            this.audit.addToAuditTable("Logged in");
        } catch(SQLException e) {
            System.out.println("Could not add to audit table");
        }
            //System.out.print("we true baby:loged on " + this.user.getUsername());
            loginCheck = !loginCheck;
        }
    }
}