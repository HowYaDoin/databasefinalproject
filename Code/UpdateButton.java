import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * handles the update button functionality
 * @author Kelsey Pereira 
 */
public class UpdateButton implements EventHandler<ActionEvent>{
    private AddToAudit audit;
    private TextField tf;
    private VBox optionBox;
    private Connection connection;
    public UpdateButton(AddToAudit a, TextField t, VBox vbox, Connection con){
        this.audit = a;
        this.tf = t;
        this.optionBox = vbox;
        this.connection = con;
    }
    @Override
    public void handle(ActionEvent arg0) {
        VBox options = (VBox)this.optionBox.getChildren().get(1);
        TextArea display = (TextArea)this.optionBox.getChildren().get(2);
        options.getChildren().clear();
        Button submit = new Button("Submit");
        options.getChildren().add(submit);
        if(this.tf.getText().equals("contributorName")) {
            this.tf.setText("");
            this.tf.setText("contributoId;name");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.updateContributorName(?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("recordingDate")){
            this.tf.setText("");
            this.tf.setText("id;date");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATERECORDINGDATE(?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("recordingDuration")){
            this.tf.setText("");
            this.tf.setText("id;duration");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATERECORDINGDURATION(?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("componentModifiedDuration")){
            this.tf.setText("");
            this.tf.setText("RecordingId;RecordingId2;componentModifiedOffset;componentModifiedDuration");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATECOMPONENTMODIFIEDDURATION(?,?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.setString(4, dataArr[3]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("componentUsedDuration")){
            this.tf.setText("");
            this.tf.setText("RecordingId;RecordingId2;componentModifiedOffset;componentModifiedDuration");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATECOMPONENTUSEDDURATION(?,?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.setString(4, dataArr[3]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("componentOffsetOriginal")){
            this.tf.setText("");
            this.tf.setText("RecordingId;RecordingId2;componentModifiedOffset;componentModifiedDuration");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATECOMPONENTOFFSETORIGINAL(?,?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.setString(4, dataArr[3]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("merchDate")){
            this.tf.setText("");
            this.tf.setText("recordingId;releaseDate;marketName");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATEMERCHDATE(?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("merchTitle")){
            this.tf.setText("");
            this.tf.setText("recordingId;title;marketName");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATEMERCHTITLE(?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("merchMarketName")){
            this.tf.setText("");
            this.tf.setText("recordingId;label;marketName");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATEMERCHMARKETNAME(?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else if (this.tf.getText().equals("merchLabel")){
            this.tf.setText("");
            this.tf.setText("recordingId;label;marketName");
            display.setText("Input required information parsed with ;\n");
            submit.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent arg0) {
                    String[] dataArr = tf.getText().split(";");
                    String sql = "call updateData_package.UPDATEMERCHLABEL(?,?,?)";
                    try{
                        CallableStatement call = connection.prepareCall(sql);
                        call.setString(1, dataArr[0]);
                        call.setString(2, dataArr[1]);
                        call.setString(3, dataArr[2]);
                        call.execute();
                        call.close();
                        display.setText("Data inserted successfully");
                    } catch (SQLException e){
                        display.setText("could not create table: invalid arguments" + e);
                    }
                    options.getChildren().clear();
                }
            });
        } else {
            display.setText("Option does not exist\nOptions are:\n-contributorName\n\n"+
            "-recordingDate\n"+
            "-recordingDuration\n\n"+
            "-componentModifiedDuration\n"+
            "-componentUsedDuration\n"+
            "-componentOffsetOriginal\n\n"+
            "-merchDate\n"+
            "-merchTitle\n"+
            "-merchMarketName\n"+
            "-merchLabel");
            options.getChildren().clear();
        }
    try{
        this.audit.addToAuditTable("Update");
    } catch(SQLException e) {
        System.out.println("Could not add to audit table");
    }
    }
}