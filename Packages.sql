--------------------------------------------
	--addData_package � Adds the "add" procedures to the database
--------------------------------------------
CREATE OR REPLACE PACKAGE addData_package AS

--------------------------------------------
--addRecordingCompilationData : This subprogram is going to allow you to create the base information for a recording or a compilation in the Recording table
--------------------------------------------

    PROCEDURE addRecordingCompilationData(
    BEGINNINGCHAR VARCHAR2,
    vRecordingDate Recording.RecordingDate%TYPE, 
    vRecordingDuration Recording.RecordingDuration%TYPE
    );
    
--------------------------------------------
--addContributor : Adds a contributor to Contributors
--------------------------------------------
    
    PROCEDURE addContributor(
    vName Contributors.Name%TYPE
    );
    
--------------------------------------------
--addRole : Adds a role to Roles
--------------------------------------------

    PROCEDURE addRole(
    vRoleName Roles.Role%TYPE
    );

--------------------------------------------
--addPerformance : Adds a performance to Performance
--------------------------------------------

    PROCEDURE addPerformance(
    vRecordingId Recording.RecordingId%TYPE, 
    vRoleName Roles.Role%TYPE,
    vContributorId Contributors.ContributorId%TYPE
    );
    
--------------------------------------------
--addComponentData : This subprogram is going to allow you to add data to Component.
--------------------------------------------
    
    PROCEDURE ADDCOMPONENTDATA(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compModifiedDuration component.durationinmodified%TYPE, 
    compOriginalOffset component.offsetinmodified%TYPE,
    compOriginalUsed component.durationused%TYPE
    );
    
-------------------------------------------
--addAuditData : add data to Auditing
-------------------------------------------
    
    PROCEDURE ADDAUDITDATA(
    VUSERNAME AUDITING.USERNAME%TYPE,
    VACTIONNAME AUDITING.ACTIONNAME%TYPE
    );
    
-------------------------------------------	
--addMerchData : add data to Merch
-------------------------------------------
    
    PROCEDURE ADDMERCHDATA(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VRELEASEDATE MERCH.RELEASEDATE%TYPE,
    VLABEL MERCH.LABEL%TYPE,
    VTITLE MERCH.TITLE%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    );
    
END addData_package;
/
CREATE OR REPLACE PACKAGE BODY addData_package AS

    PROCEDURE addRecordingCompilationData(
    BEGINNINGCHAR VARCHAR2(1),
    vRecordingDate Recording.RecordingDate%TYPE, 
    vRecordingDuration Recording.RecordingDuration%TYPE
    ) AS
        BEGIN
            IF BEGINNINGCHAR = 'R' OR BEGINNINGCHAR = 'C' THEN
                IF BEGINNINGCHAR = 'R' THEN
                    INSERT INTO Recording (RecordingId, RecordingDate, RecordingDuration)
                    VALUES (BEGINNINGCHAR||LPAD(RECORDINGRECORDING.NEXTVAL,4,'0'),vRecordingDate,vRecordingDuration);
                    DBMS_OUTPUT.PUT_LINE('Added recording '||BEGINNINGCHAR||LPAD(RECORDINGRECORDING.CURRVAL,4,'0')||' created on '||vRecordingDate||', with a duration of '||vRecordingDuration);
                ELSE 
                    INSERT INTO Recording (RecordingId, RecordingDate, RecordingDuration)
                    VALUES (BEGINNINGCHAR||LPAD(RECORDINGCOMPILATION.NEXTVAL,4,'0'),vRecordingDate,vRecordingDuration);
                    DBMS_OUTPUT.PUT_LINE('Added recording '||BEGINNINGCHAR||LPAD(RECORDINGCOMPILATION.CURRVAL,4,'0')||' created on '||vRecordingDate||', with a duration of '||vRecordingDuration);
                END IF;
            ELSE
                DBMS_OUTPUT.PUT_LINE('The beginning character must either be an R or a C');
            END IF;
        END addrecordingcompilationdata;
        
    PROCEDURE addContributor(
    vName Contributors.Name%TYPE
    ) AS
    BEGIN
            INSERT INTO Contributors
            VALUES ('CO'||LPAD(CONTRIBUTOR.NEXTVAL,3,'0'),vName);
            DBMS_OUTPUT.PUT_LINE('Added contributor '||'CO'||LPAD(CONTRIBUTOR.CURRVAL,3,'0')||' named '||vName);
    END addContributor;
    
    PROCEDURE addRole(
    vRoleName Roles.Role%TYPE
    ) AS
        ROLECHECK ROLES.ROLEID%TYPE;
    BEGIN
            SELECT COUNT(ROLEID) INTO ROLECHECK
                FROM ROLES
                    WHERE ROLE = VROLENAME;
                    
            IF ROLECHECK <= 0 THEN
                INSERT INTO Roles
                VALUES ('RO'||LPAD(ROLE.NEXTVAL,3,'0'), vRoleName);
                DBMS_OUTPUT.PUT_LINE('Added role '||'RO'||LPAD(ROLE.CURRVAL,3,'0')||' named '||vRoleName);
            ELSE
                DBMS_OUTPUT.PUT_LINE('Role name already exists!');
            END IF;
    END addRole;
    
    PROCEDURE addPerformance(
    vRecordingId Recording.RecordingId%TYPE, 
    vRoleName Roles.Role%TYPE,
    vContributorId Contributors.ContributorId%TYPE
    ) AS
    recordingCheck NUMBER(1);
    roleNameCheck NUMBER(1);
    contributorCheck NUMBER(1);
    VROLEID ROLES.ROLEID%TYPE;
    BEGIN
            SELECT COUNT(RecordingId) INTO recordingCheck 
                FROM Recording
                    WHERE RecordingId = vRecordingId;
            SELECT COUNT(Role) INTO roleNameCheck
                FROM Roles
                    WHERE Role = vRoleName;
            SELECT COUNT(CONTRIBUTORID) INTO contributorCheck
                FROM CONTRIBUTORS
                    WHERE CONTRIBUTORID = VCONTRIBUTORID;
                    IF recordingCheck > 0 THEN
                        IF contributorCheck > 0 THEN
                            IF roleNameCheck > 0 THEN
                                SELECT ROLEID INTO VROLEID
                                FROM ROLES
                                    WHERE ROLE = VROLENAME;
                                INSERT INTO Performance
                                VALUES (vRecordingId, vContributorId, VROLEID);
                                DBMS_OUTPUT.PUT_LINE('Added performance with recording '||vRecordingId||' with contributor '||vContributorId||' with role '||VROLEID);
                            ELSE
                               DBMS_OUTPUT.PUT_LINE('The role does not exist!');
                            END IF;
                        ELSE
                            DBMS_OUTPUT.PUT_LINE('The contributor does not exist!');
                        END IF;
                    ELSE
                        DBMS_OUTPUT.PUT_LINE('Such a recording does not exist!');
                    END IF;
    END addPerformance;
    
    PROCEDURE ADDCOMPONENTDATA(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compModifiedDuration component.durationinmodified%TYPE, 
    compOriginalOffset component.offsetinmodified%TYPE,
    compOriginalUsed component.durationused%TYPE
    ) AS
    dupCheck NUMBER(1);
    recordingCheck NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO DUPCHECK
        FROM COMPONENT
            WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINMODIFIED = COMPMODIFIEDOFFSET;
        SELECT COUNT(RecordingId) INTO recordingCheck 
        FROM Recording
            WHERE RecordingId = vRecordingId;
            IF SUBSTR(vRecordingId,1,1) = 'C' THEN
                IF recordingCheck <= 0 THEN
                    DBMS_OUTPUT.PUT_LINE('RecordingId does not exist!');
                ELSE
                    IF DUPCHECK <= 0 THEN
                        INSERT INTO Component (RecordingId, OffsetInModified, DurationInModified, PartId, OffsetInOriginal, DurationUsed)
                        VALUES (vRecordingId,compModifiedOffset,compModifiedDuration,vRecordingId2,compOriginalOffset,compOriginalUsed);
                    ELSE
                        DBMS_OUTPUT.PUT_LINE('Compilation already exists');
                    END IF;
                END IF;
            ELSE
                DBMS_OUTPUT.PUT_LINE('The id must be a compilation');
            END IF;
    END addComponentData;
    
    PROCEDURE ADDAUDITDATA(
    VUSERNAME AUDITING.USERNAME%TYPE,
    VACTIONNAME AUDITING.ACTIONNAME%TYPE
    )
    AS
    BEGIN
        INSERT INTO AUDITING
        VALUES (VUSERNAME,VACTIONNAME,EXTRACT(YEAR FROM SYSDATE)||'-'||EXTRACT(MONTH FROM SYSDATE)
        ||'-'||EXTRACT(DAY FROM SYSDATE),EXTRACT(HOUR FROM SYSTIMESTAMP)||'-'||
        EXTRACT(MINUTE FROM SYSTIMESTAMP)||'-'||EXTRACT(SECOND FROM SYSTIMESTAMP));
        DBMS_OUTPUT.PUT_LINE('Added audit for'||VUSERNAME);
    END addAuditData;
    
    PROCEDURE ADDMERCHDATA(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VRELEASEDATE MERCH.RELEASEDATE%TYPE,
    VLABEL MERCH.LABEL%TYPE,
    VTITLE MERCH.TITLE%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    )
    AS
        RECORDINGCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO RECORDINGCHECK
        FROM RECORDING
            WHERE RECORDINGID = VRECORDINGID;
        IF RECORDINGCHECK > 0 THEN
            INSERT INTO MERCH
            VALUES(VRECORDINGID, VRELEASEDATE,VLABEL, VTITLE, VMARKETNAME);
            DBMS_OUTPUT.PUT_LINE('Added merch for '||VRECORDINGID||' with the title of '||VTITLE);
        ELSE
            DBMS_OUTPUT.PUT_LINE('The recording/compilation id does not exist');
        END IF;
    END addMerchData;
    
END addData_package;
/

--------------------------------------------
	--updateData_package � Adds the "update" procedures to the database
--------------------------------------------

CREATE OR REPLACE PACKAGE updateData_package AS
    
--------------------------------------------
--updateContributorName : Updates the Name of a contributor
--------------------------------------------
    
    PROCEDURE updateContributorName(
    VCONTRIBUTORID CONTRIBUTORS.CONTRIBUTORID%TYPE,
    VNAME CONTRIBUTORS.NAME%TYPE
    );
    
--------------------------------------------
--updateRecordingDate : Updates the RecordingDate of data in Recording
--------------------------------------------	
    
    PROCEDURE UPDATERECORDINGDATE(
    ID VARCHAR2, VDATE DATE
    );
    
--------------------------------------------
--updateRecordingDuration : Updates the RecdordingDuration of data in Recording
--------------------------------------------
    
     PROCEDURE UPDATERECORDINGDURATION(
    ID VARCHAR2, VDURATION VARCHAR2
    );
    
--------------------------------------------	
--updateComponentModifiedDuration : Updates the modified duration of the chosen row in Component
--------------------------------------------
    
    PROCEDURE UPDATECOMPONENTMODIFIEDDURATION(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compModifiedDuration component.durationinmodified%TYPE
    );
    
--------------------------------------------	
--updateComponentUsedDuration : Updates the used duration of the chosen row in Component
--------------------------------------------
    
    PROCEDURE UPDATECOMPONENTUSEDDURATION(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compUsedDuration component.durationinmodified%TYPE
    );
    
--------------------------------------------	
--updateComponentOffsetOriginal : Updates the offsetinOriginal column in Component
--------------------------------------------
    
    PROCEDURE UPDATECOMPONENTOFFSETORIGINAL(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compOriginalOffset component.durationinmodified%TYPE
    );
    
-------------------------------------------
--updateMerchDate : update ReleaseDate of Merch
-------------------------------------------
    
    PROCEDURE UPDATEMERCHDATE(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VRELEASEDATE RECORDING.RECORDINGDATE%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    );
    
-------------------------------------------	
--updateMerchTitle : update Title of Merch
-------------------------------------------    

    PROCEDURE UPDATEMERCHTITLE(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VTITLE MERCH.TITLE%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    );
    
-------------------------------------------	
--updateMerchMarketName : update the MarketName of Merch
-------------------------------------------
    
    PROCEDURE UPDATEMERCHMARKETNAME(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VLABEL MERCH.LABEL%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    );
    
-------------------------------------------	
--updateMerchLabel : update the Label of Merch
-------------------------------------------
    
    PROCEDURE UPDATEMERCHLABEL(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VLABEL MERCH.LABEL%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    );
END;
/
CREATE OR REPLACE PACKAGE BODY updateData_package AS
    PROCEDURE updateContributorName(
    VCONTRIBUTORID CONTRIBUTORS.CONTRIBUTORID%TYPE,
    VNAME CONTRIBUTORS.NAME%TYPE
    )
    AS
        CONTRIBUTORCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(CONTRIBUTORID) INTO CONTRIBUTORCHECK
        FROM CONTRIBUTORS
            WHERE CONTRIBUTORID = VCONTRIBUTORID;
        IF CONTRIBUTORCHECK > 0 THEN
            UPDATE CONTRIBUTORS
            SET NAME = VNAME
            WHERE CONTRIBUTORID = VCONTRIBUTORID;
            DBMS_OUTPUT.PUT_LINE('Contributor '||VCONTRIBUTORID||' got their name changed to '||VNAME);
        ELSE
            DBMS_OUTPUT.PUT_LINE('The contributor id does not exist');
        END IF;
    END;
    
    PROCEDURE UPDATERECORDINGDATE(
    ID VARCHAR2, VDATE DATE
    )
    AS
    IDCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO IDCHECK
        FROM RECORDING
            WHERE RECORDINGID = ID;
            IF IDCHECK > 0 THEN
                UPDATE RECORDING
                SET RECORDINGDATE = VDATE
                WHERE RECORDINGID = ID;
            ELSE
                DBMS_OUTPUT.PUT_LINE('The id does not exist. Choose one that exists.');
            END IF;
    END;
    
    PROCEDURE UPDATERECORDINGDURATION(
    ID VARCHAR2, VDURATION VARCHAR2
    )
    AS
    IDCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO IDCHECK
        FROM RECORDING
            WHERE RECORDINGID = ID;
            IF IDCHECK > 0 THEN
                UPDATE RECORDING
                SET RECORDINGDURATION = VDURATION
                WHERE RECORDINGID = ID;
            ELSE
                DBMS_OUTPUT.PUT_LINE('The id does not exist. Choose one that exists.');
            END IF;
    END;
    
    PROCEDURE UPDATECOMPONENTMODIFIEDDURATION(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compModifiedDuration component.durationinmodified%TYPE
    ) AS
    existCheck NUMBER(1);
    dupCheck NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO DUPCHECK
        FROM COMPONENT
            WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND DURATIONINMODIFIED = COMPMODIFIEDDURATION;
        SELECT COUNT(RecordingId) INTO existCheck
        FROM COMPONENT
            WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINMODIFIED = COMPMODIFIEDOFFSET;
            IF EXISTCHECK > 0 THEN
                IF DUPCHECK <= 0 THEN
                    UPDATE COMPONENT
                    SET DURATIONINMODIFIED = COMPMODIFIEDDURATION
                    WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINMODIFIED = COMPMODIFIEDOFFSET;
                ELSE
                    DBMS_OUTPUT.PUT_LINE('The target component already exists');
                END IF;
            ELSE
                DBMS_OUTPUT.PUT_LINE('The component to be modified does not exist');
            END IF;
    END;
    
    PROCEDURE UPDATECOMPONENTUSEDDURATION(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compUsedDuration component.durationinmodified%TYPE
    ) AS
    existCheck NUMBER(1);
    dupCheck NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO DUPCHECK
        FROM COMPONENT
            WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND DURATIONUSED = COMPUSEDDURATION;
        SELECT COUNT(RecordingId) INTO existCheck
        FROM COMPONENT
            WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINMODIFIED = COMPMODIFIEDOFFSET;
            IF EXISTCHECK > 0 THEN
                IF DUPCHECK <= 0 THEN
                    UPDATE COMPONENT
                    SET DURATIONUSED = COMPUSEDDURATION
                    WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINMODIFIED = COMPMODIFIEDOFFSET;
                ELSE
                    DBMS_OUTPUT.PUT_LINE('The target component already exists');
                END IF;
            ELSE
                DBMS_OUTPUT.PUT_LINE('The component to be modified does not exist');
            END IF;
    END;
    
    PROCEDURE UPDATECOMPONENTOFFSETORIGINAL(
    vRecordingId Recording.RecordingId%TYPE,
    vRecordingId2 Recording.RecordingId%TYPE,
    compModifiedOffset component.offsetinmodified%TYPE,
    compOriginalOffset component.durationinmodified%TYPE
    ) AS
    existCheck NUMBER(1);
    dupCheck NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO DUPCHECK
        FROM COMPONENT
            WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINORIGINAL = COMPORIGINALOFFSET;
        SELECT COUNT(RecordingId) INTO existCheck
        FROM COMPONENT
            WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINMODIFIED = COMPMODIFIEDOFFSET;
            IF EXISTCHECK > 0 THEN
                IF DUPCHECK <= 0 THEN
                    UPDATE COMPONENT
                    SET OFFSETINORIGINAL = COMPORIGINALOFFSET
                    WHERE RECORDINGID = VRECORDINGID AND PARTID = VRECORDINGID2 AND OFFSETINMODIFIED = COMPMODIFIEDOFFSET;
                ELSE
                    DBMS_OUTPUT.PUT_LINE('The target component already exists');
                END IF;
            ELSE
                DBMS_OUTPUT.PUT_LINE('The component to be modified does not exist');
            END IF;
    END;
    
    PROCEDURE UPDATEMERCHDATE(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VRELEASEDATE RECORDING.RECORDINGDATE%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    )
    AS
        RECORDINGCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO RECORDINGCHECK
        FROM MERCH
            WHERE RECORDINGID = VRECORDINGID AND MARKETNAME = VMARKETNAME;
        IF RECORDINGCHECK > 0 THEN
            UPDATE MERCH
            SET RELEASEDATE = VRELEASEDATE 
            WHERE RECORDINGID = VRECORDINGID AND MARKETNAME = VMARKETNAME; 
            DBMS_OUTPUT.PUT_LINE('Updated Merch for '||VRECORDINGID||' in market '||VMARKETNAME||' with the new ReleaseDate '||VRELEASEDATE);
        ELSE
            DBMS_OUTPUT.PUT_LINE('The recording/compilation does not exist');
        END IF;
    END;
    
    PROCEDURE UPDATEMERCHTITLE(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VTITLE MERCH.TITLE%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    )
    AS
        RECORDINGCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO RECORDINGCHECK
        FROM MERCH
            WHERE RECORDINGID = VRECORDINGID AND MARKETNAME = VMARKETNAME;
        IF RECORDINGCHECK > 0 THEN
            UPDATE MERCH
            SET TITLE = VTITLE
            WHERE RECORDINGID = VRECORDINGID AND MARKETNAME = VMARKETNAME; 
            DBMS_OUTPUT.PUT_LINE('Updated Merch for '||VRECORDINGID||' in market '||VMARKETNAME||' with the new Title '||VTITLE);
        ELSE
            DBMS_OUTPUT.PUT_LINE('The recording/compilation does not exist');
        END IF;
    END;
    
    PROCEDURE UPDATEMERCHMARKETNAME(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VLABEL MERCH.LABEL%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    )
    AS
        RECORDINGCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO RECORDINGCHECK
        FROM MERCH
            WHERE RECORDINGID = VRECORDINGID AND LABEL = VLABEL;
        IF RECORDINGCHECK > 0 THEN
            UPDATE MERCH
            SET MARKETNAME = VMARKETNAME
            WHERE RECORDINGID = VRECORDINGID AND LABEL = VLABEL; 
            DBMS_OUTPUT.PUT_LINE('Updated Merch for '||VRECORDINGID||' with label '||VLABEL||' with the new MarketName '||VMARKETNAME);
        ELSE
            DBMS_OUTPUT.PUT_LINE('The recording/compilation does not exist');
        END IF;
    END;
    
    PROCEDURE UPDATEMERCHLABEL(
    VRECORDINGID RECORDING.RECORDINGID%TYPE,
    VLABEL MERCH.LABEL%TYPE,
    VMARKETNAME MERCH.MARKETNAME%TYPE
    )
    AS
        RECORDINGCHECK NUMBER(1);
    BEGIN
        SELECT COUNT(RECORDINGID) INTO RECORDINGCHECK
        FROM MERCH
            WHERE RECORDINGID = VRECORDINGID AND MARKETNAME = VMARKETNAME;
        IF RECORDINGCHECK > 0 THEN
            UPDATE MERCH
            SET LABEL = VLABEL
            WHERE RECORDINGID = VRECORDINGID AND MARKETNAME = VMARKETNAME; 
            DBMS_OUTPUT.PUT_LINE('Updated Merch for '||VRECORDINGID||' in market '||VMARKETNAME||' with the new Label '||VLABEL);
        ELSE
            DBMS_OUTPUT.PUT_LINE('The recording/compilation does not exist');
        END IF;
    END;

END updateData_package;
/

--------------------------------------------		
	--selectData_package � Selects data from the database
--------------------------------------------

create or replace PACKAGE selectData_package AS

--------------------------------------------
--selectContributorRoles : Shows all the roles of a contributor for a specific recording/compilation
--------------------------------------------

    PROCEDURE SELECTCONTRIBUTORROLES(
    VSEARCHCONTRIBUTOR VARCHAR2, 
    VSEARCHRECORDING VARCHAR2,
    VSTRING OUT VARCHAR2
    );

--------------------------------------------
--selectRecordingContributorsData : Shows the contributors and their used role for a comp/rec
--------------------------------------------

    PROCEDURE SELECTRECORDINGCONTRIBUTORSDATA(VSEARCHRECORDING VARCHAR2, VSTRING OUT VARCHAR2);

--------------------------------------------
--selectContributorsComponent : Shows the contributors and their used role for the components of a compilation
--------------------------------------------

    PROCEDURE SELECTCONTRIBUTORSCOMPONENT(VSEARCHRECORDINGID VARCHAR2, VSTRING OUT VARCHAR2);

--------------------------------------------
--selectRecordingDetails : Shows the date and duration of a recording, as well as the Compilations it is a part of. If a compilation is chosen, it calls selectComponentDetails
--------------------------------------------

    PROCEDURE SELECTRECORDINGDETAILS(VSEARCHRECORDINGID VARCHAR2, VSTRING OUT VARCHAR2);

--------------------------------------------
--selectComponentDetails : Shows all the info in the Component table of a compilation
--------------------------------------------

    PROCEDURE SELECTCOMPONENTDETAILS(VSEARCHRECORDINGID VARCHAR2, VSTRING OUT VARCHAR2);

--------------------------------------------
--selectRecordingRolesData : This subprogram is going to allow us to list all of the recordings/compilations a contributor has participated in and their roles in each of them.
--------------------------------------------

    PROCEDURE SELECTRECORDINGROLESDATA(VSEARCHCONTRIBUTOR VARCHAR2, VSTRING OUT VARCHAR2);

--------------------------------------------
--selectAllRolesOfContributor: Shows all the roles of a contributor
--------------------------------------------

    PROCEDURE SELECTALLROLESOFCONTRIBUTOR(VSEARCHCONTRIBUTOR VARCHAR2, VSTRING OUT VARCHAR2);

--------------------------------------------
--selectAllRecordings: Shows all the recordings in the recordings table
--------------------------------------------

    PROCEDURE SELECTALLRECORDINGS(VSTRING OUT VARCHAR2);

--------------------------------------------
--selectAllCompilations: Shows all the compilations in the recordings table
--------------------------------------------

    PROCEDURE SELECTALLCOMPILATIONS(VSTRING OUT VARCHAR2);

--------------------------------------------
--selectAllMarketedRecordings: Shows all the recordings in the Merch table
--------------------------------------------

    PROCEDURE SELECTALLMARKETEDRECORDINGS(VSTRING OUT VARCHAR2);

--------------------------------------------
--selectAllMarketedCompilations: Shows all the compilations in the Merch table
--------------------------------------------

    PROCEDURE SELECTALLMARKETEDCOMPILATIONS(VSTRING OUT VARCHAR2);

END selectData_package;
/
create or replace PACKAGE BODY selectData_package AS

    PROCEDURE SELECTCONTRIBUTORROLES(
    VSEARCHCONTRIBUTOR VARCHAR2, 
    VSEARCHRECORDING VARCHAR2,
    VSTRING OUT VARCHAR2
    )
    AS
    CURSOR CRESULT IS
    SELECT ROLE FROM ROLES
        INNER JOIN PERFORMANCE
        USING (ROLEID)
        WHERE CONTRIBUTORID = VSEARCHCONTRIBUTOR AND RECORDINGID = VSEARCHRECORDING;
    VRESULT ROLES.ROLE%TYPE;
    BEGIN
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRESULT;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
            END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        VSTRING := VSTRING||'Role(s): '||VRESULT||CHR(10);
        END LOOP;
        CLOSE CRESULT;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        VSTRING := VSTRING||'No data found for roles'||CHR(10);
    END;

    PROCEDURE SELECTRECORDINGCONTRIBUTORSDATA(VSEARCHRECORDING VARCHAR2, VSTRING OUT VARCHAR2)
    AS
    CURSOR CRESULT IS
    SELECT CONTRIBUTORID FROM Contributors
        INNER JOIN PERFORMANCE
        USING (ContributorId)
        WHERE RecordingId = VSEARCHRECORDING;
    VRESULT CONTRIBUTORS.NAME%TYPE;
    VSTRING2 VARCHAR2(20);
    VCONTRIBUTORNAME CONTRIBUTORS.CONTRIBUTORID%TYPE;
    BEGIN
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRESULT;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
            END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        SELECT NAME INTO VCONTRIBUTORNAME
            FROM CONTRIBUTORS
                WHERE CONTRIBUTORID = VRESULT;
            VSTRING := VSTRING||'For id '||VSEARCHRECORDING||CHR(10);
            VSTRING := VSTRING||'Contributor: '||VCONTRIBUTORNAME||CHR(10);
            SELECTCONTRIBUTORROLES(VRESULT, VSEARCHRECORDING, VSTRING2);
            VSTRING := VSTRING||VSTRING2;
        END LOOP;
        CLOSE CRESULT;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        VSTRING := VSTRING||'No data found for this contributorid'||CHR(10);
    END;

    PROCEDURE SELECTCONTRIBUTORSCOMPONENT(VSEARCHRECORDINGID VARCHAR2, VSTRING OUT VARCHAR2)
    AS
    VRESULT2 VARCHAR2(300);
    CURSOR CRESULT IS
    SELECT PARTID FROM COMPONENT
        WHERE RECORDINGID = VSEARCHRECORDINGID;
    VRESULT COMPONENT.PARTID%TYPE;
    BEGIN
        VSTRING := VSTRING||'Components of '||VSEARCHRECORDINGID||':'||CHR(10);
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRESULT;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
            END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        SELECTRECORDINGCONTRIBUTORSDATA(VRESULT,VRESULT2);
        VSTRING := VSTRING||VRESULT2||CHR(10);
        END LOOP;
        CLOSE CRESULT;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        VSTRING := VSTRING||'No data found for the components of the specified compilation id'||CHR(10);
    END;

    PROCEDURE SELECTRECORDINGDETAILS(VSEARCHRECORDINGID VARCHAR2, VSTRING OUT VARCHAR2)
    AS
    VRESULT VARCHAR2(300);
    VDATE recording.recordingdate%TYPE;
    VDURATION recording.recordingduration%TYPE;
    BEGIN
        SELECT RECORDINGDATE INTO VDATE 
            FROM RECORDING
                WHERE RECORDINGID = VSEARCHRECORDINGID;
        SELECT RECORDINGDURATION INTO VDURATION
            FROM RECORDING
                WHERE RECORDINGID = VSEARCHRECORDINGID;
        VSTRING := VSTRING||VSEARCHRECORDINGID||' was created on '||VDATE||' and has a duration of '||VDURATION||CHR(10);
        IF SUBSTR(vsearchrecordingid,1,1) = 'C' THEN
            SELECTCOMPONENTDETAILS(VSEARCHRECORDINGID, VRESULT);
            VSTRING := VSTRING||VRESULT||CHR(10);
        END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        VSTRING := VSTRING||'Recording id does not exist'||CHR(10);
    END;

    PROCEDURE SELECTCOMPONENTDETAILS(VSEARCHRECORDINGID VARCHAR2, VSTRING OUT VARCHAR2)
    AS
    CURSOR CRESULT IS
    SELECT * FROM COMPONENT
        WHERE RECORDINGID = VSEARCHRECORDINGID;
    VRECORDINGID COMPONENT.RECORDINGID%TYPE;
    VOFFSETM COMPONENT.OFFSETINMODIFIED%TYPE;
    VDURATIONM COMPONENT.DURATIONINMODIFIED%TYPE;
    VPARTID COMPONENT.PARTID%TYPE;
    VOFFSETO COMPONENT.OFFSETINORIGINAL%TYPE;
    VDURATIONO COMPONENT.DURATIONUSED%TYPE;
    BEGIN
        VSTRING := VSTRING||'Components of '||VSEARCHRECORDINGID||':'||CHR(10);
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRECORDINGID, VOFFSETM, VDURATIONM, VPARTID, VOFFSETO, VDURATIONO;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
            END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        VSTRING := VSTRING||VPARTID||' -> Offset in Compilation: '||VOFFSETM||', Modified Clipped Duration: '||VDURATIONM||', Offset in Original: '||VOFFSETO||', Original Clipped Duration: '||VDURATIONO||CHR(10);
        END LOOP;
        CLOSE CRESULT;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        VSTRING := VSTRING||'No data found in component for that compilation id'||CHR(10);
    END;

    PROCEDURE SELECTRECORDINGROLESDATA(VSEARCHCONTRIBUTOR VARCHAR2, VSTRING OUT VARCHAR2)
    AS
    VRESULT2 VARCHAR2(300);
    CURSOR CRESULT IS
    SELECT RecordingId FROM Recording
        INNER JOIN PERFORMANCE
        USING (RecordingId)
        WHERE ContributorId = VSEARCHCONTRIBUTOR;
    VRESULT RECORDING.RECORDINGID%TYPE;
    BEGIN
        VSTRING := VSTRING||'Contributor '||VSEARCHCONTRIBUTOR||' has contributed in:'||CHR(10);
        OPEN CRESULT;
        LOOP
            FETCH CRESULT INTO VRESULT;
            IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
            END IF;
            EXIT WHEN CRESULT%NOTFOUND;
            VSTRING := VSTRING||VRESULT||CHR(10);
            SELECTCONTRIBUTORROLES(VSEARCHCONTRIBUTOR, VRESULT, VRESULT2);
            VSTRING := VSTRING||VRESULT2||CHR(10);
        END LOOP;
        CLOSE CRESULT;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     VSTRING := VSTRING||'Not a valid contributor id'||CHR(10);
    END;

    PROCEDURE SELECTALLROLESOFCONTRIBUTOR(VSEARCHCONTRIBUTOR VARCHAR2, VSTRING OUT VARCHAR2)
    AS
    CURSOR CRESULT IS
    SELECT DISTINCT ROLEID FROM PERFORMANCE
        WHERE CONTRIBUTORID = VSEARCHCONTRIBUTOR;
    VRESULTROLEID PERFORMANCE.ROLEID%TYPE;
    VRESULTROLE ROLES.ROLE%TYPE;
    BEGIN
        VSTRING := VSTRING||'All roles played by '||VSEARCHCONTRIBUTOR||':'||CHR(10);
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRESULTROLEID;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
            END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        SELECT ROLE INTO VRESULTROLE
        FROM ROLES
            WHERE ROLEID = VRESULTROLEID;
        VSTRING := VSTRING||VRESULTROLE||CHR(10);
        END LOOP;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     VSTRING := VSTRING||'Not a valid contributor id'||CHR(10);
    END;

    PROCEDURE SELECTALLRECORDINGS(VSTRING OUT VARCHAR2)
    AS
    CURSOR CRESULT IS
    SELECT * FROM RECORDING
        WHERE SUBSTR(RECORDINGID,1,1) = 'R';
    VRECORDINGID RECORDING.RECORDINGID%TYPE;
    VRECORDINGDATE RECORDING.RECORDINGDATE%TYPE;
    VRECORDINGDURATION RECORDING.RECORDINGDURATION%TYPE;
    BEGIN
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRECORDINGID, VRECORDINGDATE, VRECORDINGDURATION;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
        END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        VSTRING := VSTRING||VRECORDINGID||' was created on '||VRECORDINGDATE||' and has a duration of '||VRECORDINGDURATION||CHR(10);
        END LOOP;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     VSTRING := VSTRING||'No data found'||CHR(10);
    END;

    PROCEDURE SELECTALLCOMPILATIONS(VSTRING OUT VARCHAR2)
    AS
    CURSOR CRESULT IS
    SELECT * FROM RECORDING
        WHERE SUBSTR(RECORDINGID,1,1) = 'C';
    VRECORDINGID RECORDING.RECORDINGID%TYPE;
    VRECORDINGDATE RECORDING.RECORDINGDATE%TYPE;
    VRECORDINGDURATION RECORDING.RECORDINGDURATION%TYPE;
    BEGIN
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRECORDINGID, VRECORDINGDATE, VRECORDINGDURATION;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
        END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        VSTRING := VSTRING||VRECORDINGID||' was created on '||VRECORDINGDATE||' and has a duration of '||VRECORDINGDURATION||CHR(10);
        END LOOP;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     VSTRING := VSTRING||'No data found'||CHR(10);
    END;

    PROCEDURE SELECTALLMARKETEDRECORDINGS(VSTRING OUT VARCHAR2)
    AS
    CURSOR CRESULT IS
    SELECT * FROM MERCH
        WHERE SUBSTR(RECORDINGID,1,1) = 'R';
    VRECORDINGID RECORDING.RECORDINGID%TYPE;
    VRELEASEDATE RECORDING.RECORDINGDATE%TYPE;
    VLABEL MERCH.LABEL%TYPE;
    VTITLE MERCH.TITLE%TYPE;
    VMARKETNAME MERCH.MARKETNAME%TYPE;
    BEGIN
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRECORDINGID, VRELEASEDATE, VLABEL, VTITLE, VMARKETNAME;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
        END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        VSTRING := VSTRING||VRECORDINGID||' was released on '||VRELEASEDATE||' through the label '||VLABEL||' with the title "'||VTITLE||'" through the '||VMARKETNAME||' market'||CHR(10);
        END LOOP;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     VSTRING := VSTRING||'No data found'||CHR(10);
    END;

    PROCEDURE SELECTALLMARKETEDCOMPILATIONS(VSTRING OUT VARCHAR2)
    AS
    CURSOR CRESULT IS
    SELECT * FROM MERCH
        WHERE SUBSTR(RECORDINGID,1,1) = 'C';
    VRECORDINGID RECORDING.RECORDINGID%TYPE;
    VRELEASEDATE RECORDING.RECORDINGDATE%TYPE;
    VLABEL MERCH.LABEL%TYPE;
    VTITLE MERCH.TITLE%TYPE;
    VMARKETNAME MERCH.MARKETNAME%TYPE;
    BEGIN
        OPEN CRESULT;
        LOOP
        FETCH CRESULT INTO VRECORDINGID, VRELEASEDATE, VLABEL, VTITLE, VMARKETNAME;
        IF CRESULT%ROWCOUNT = 0 THEN
                RAISE NO_DATA_FOUND;
        END IF;
        EXIT WHEN CRESULT%NOTFOUND;
        VSTRING := VSTRING||VRECORDINGID||' was released on '||VRELEASEDATE||' through the label '||VLABEL||' with the title "'||VTITLE||'" through the '||VMARKETNAME||' market'||CHR(10);
        END LOOP;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     VSTRING := VSTRING||'No data found'||CHR(10);
    END;

END selectData_package;
/

--------------------------------------------
	--deletaData_package � Adds the "delete" procedures to the database
--------------------------------------------

CREATE OR REPLACE PACKAGE deleteData_package AS
    
--------------------------------------------
--deleteRecordingData : This subprogram is going to remove certain recordings that are specified by the user.
--------------------------------------------
    
    PROCEDURE DELETERECORDINGDATA (
        idToDelete recording.RecordingId%TYPE
    );

-------------------------------------------	
--deletePerformance : delete data of Performance
-------------------------------------------

    PROCEDURE DELETEPERFORMANCE (
        idToDelete recording.RecordingId%TYPE
    );

-------------------------------------------	
--deleteMerchData : delete data of Merch
-------------------------------------------

    PROCEDURE DELETEMERCH (
        idToDelete recording.RecordingId%TYPE
    );

--------------------------------------------	
--deleteComponents : This subprogram is going to remove components
-------------------------------------------

    PROCEDURE DELETECOMPONENTS (
        idToDelete recording.RecordingId%TYPE
    );

--------------------------------------------
--deleteUnused: Removes unused Contributors and Roles that aren't in Performance
--------------------------------------------

    PROCEDURE DELETEUNUSED;

END deleteData_package;
/
CREATE OR REPLACE PACKAGE BODY deleteData_package AS
        
    PROCEDURE DELETERECORDINGDATA (
        idToDelete recording.RecordingId%TYPE
    ) AS
    BEGIN
        DELETE FROM recording WHERE RecordingId = idToDelete;
        DELETE FROM component WHERE RecordingId = idToDelete;
        DELETE FROM merch WHERE RecordingId = idToDelete;
        DELETE FROM performance WHERE RecordingId = idToDelete;
        DELETE FROM component WHERE PartId = idToDelete;

    EXCEPTION
        WHEN no_data_found THEN
            DBMS_OUTPUT.PUT_LINE('This Recording id does not exist '||idToDelete);
    END;

    PROCEDURE DELETEPERFORMANCE (
        idToDelete recording.RecordingId%TYPE
    ) AS
    BEGIN
        DELETE FROM PERFORMANCE WHERE RecordingId = idToDelete;
    EXCEPTION
        WHEN no_data_found THEN
            DBMS_OUTPUT.PUT_LINE('This Recording id does not exist '||idToDelete);
    END;

    PROCEDURE DELETEMERCH (
        idToDelete recording.RecordingId%TYPE
    ) AS
    BEGIN
        DELETE FROM MERCH WHERE RecordingId = idToDelete;
    EXCEPTION
        WHEN no_data_found THEN
            DBMS_OUTPUT.PUT_LINE('This Recording id does not exist '||idToDelete);
    END;

    PROCEDURE DELETECOMPONENTS (
        idToDelete recording.RecordingId%TYPE
    ) AS
    BEGIN
        DELETE FROM Component WHERE RecordingId = idToDelete;
        DELETERECORDINGDATA(idToDelete);
    EXCEPTION
        WHEN no_data_found THEN
            DBMS_OUTPUT.PUT_LINE('This Recording id does not exist '||idToDelete);
    END;

    PROCEDURE DELETEUNUSED AS
    BEGIN
        DELETE FROM contributors WHERE NOT EXISTS (SELECT * FROM performance WHERE contributorId IN (SELECT ContributorId FROM contributors));
        DELETE FROM roles WHERE NOT EXISTS (SELECT * FROM performance WHERE roleId IN (SELECT roleId FROM roles));
    END;

END deleteData_package;
/