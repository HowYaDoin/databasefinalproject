import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * 
 */
public class SubmitButton implements EventHandler<ActionEvent>{
    private AddToAudit audit;
    private TextField tf;
    public SubmitButton(AddToAudit audit, TextField tf){
        this.audit = audit;
        this.tf = tf;
    }
    @Override
    public void handle(ActionEvent arg0) {
    try{
        this.audit.addToAuditTable("Select");
    } catch(SQLException e) {
        System.out.println("Could not add to audit table");
    }
    }
}
