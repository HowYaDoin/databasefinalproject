import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * adds to the audit log
 * @author Kelsey Pereira
 */
public class AddToAudit {
    private Login loginName;
    private Connection connection;
    public AddToAudit(Login name, Connection con){
        this.loginName = name;
        this.connection = con;
    }
    /**
     * returns login name
     * @param loginName
     */
    public void setLoginName(Login loginName) {
        this.loginName = loginName;
    }
    /**
     * adds to the audit table using a stored procedure
     * @param action
     * @throws SQLException
     */
    public void addToAuditTable(String action) throws SQLException{
        System.out.println("" + this.loginName.getUsername( )+ " | " + action );
        String auditString = "call addData_package.ADDAUDITDATA(?,?)";
            CallableStatement auditCall = this.connection.prepareCall(auditString);
            auditCall.setString(1, this.loginName.getUsername());
            auditCall.setString(2, action);
            auditCall.execute();
            auditCall.close();
    }
}
